Service for entity spotting. Based on BERT (bert-base-german-cased; https://deepset.ai/german-bert) and trained on the WikiNER corpus (Nothman et al., 2012).

Note that this code works for German only.
As input it expects either plaintext or NIF in n3/turtle format, and outputs NIF in n3/turtle format (more format support on the TODO list)